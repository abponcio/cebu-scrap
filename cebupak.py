from lxml import html
from string import ascii_lowercase
import requests
import threading
import time

start = 1
end = 31

months = [4]
days = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,21,26,31]
airports = ['DPS','SIN','TPE','HKG','KUL']
price_range = 4000

print('------- SEARCHING RATE LESS THAN ' + str(price_range) + ' ----------')
def check_price(airport, returnFlight):
    for month in months:
        for day in days:    
            if returnFlight == 'true':
                page = requests.get("https://beta.cebupacificair.com/Flight/InternalSelect?s=true&o1=" + airport + "&d1=MNL&dd1=2018-" + str(month) + "-" + str(day) + "&mon=true")
            else:
                page = requests.get("https://beta.cebupacificair.com/Flight/InternalSelect?s=true&o1=MNL&d1=" + airport  + "&dd1=2018-" + str(month) + "-" + str(day) + "&mon=true")
                
            tree = html.fromstring(page.content)
            m = tree.xpath("//div[contains(@class,'flights-schedule-col')]/a/p[contains(@class,'month')]/text()")
            d = tree.xpath("//div[contains(@class,'flights-schedule-col')]/a/p[contains(@class,'day')]/text()")
            f = tree.xpath("//div[contains(@class,'flights-schedule-col')]/a/p[contains(@class,'lowFareAmt')]/text()")
            x = 0
            for amt in f:
                amount = 0
                if (int(d[x]) == day and f[x] is not None and f[x] != 'Unavailable'):
                    if (float(f[x].replace(',','')) <= price_range):
                        print airport + ': ' + str(m[x]) + ' - ' + str(d[x]) + ' - ' + str(f[x])
                
                x = x + 1
            
            time.sleep(1)

threads = []
returnThread = []
for airport in airports:
    t = threading.Thread(target=check_price, args=(airport,'false',))
    threads.append(t)
    t.start()

print('DEPART')
for tr in threads:
    tr.join()

for airport in airports:
    t = threading.Thread(target=check_price, args=(airport,'true',))
    returnThread.append(t)
    t.start()

print('RETURN')
for tr in returnThread:
    tr.join()
