from lxml import html
from string import ascii_lowercase
import requests
import threading
import time

start = 1
end = 31

months = [4]
days = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,21,26,31]
airports = ['SIN']

def check_price(airport):
    for month in months:
        for day in days:
            page = requests.get("https://www.jetstar.com/ph/en/search-flights?origin=MNL&destination=" + airport + "&flight-type=2&adult=1&flexible=1&currency=PHP&sale-id=9dd7e6a8-1e55-4d05-8d3c-7a1f2c896292");
            tree = html.fromstring(page.content)
            m = tree.xpath("//div[contains(@class,'flights-schedule-col')]/a/p[contains(@class,'month')]/text()")
            d = tree.xpath("//div[contains(@class,'flights-schedule-col')]/a/p[contains(@class,'day')]/text()")
            f = tree.xpath("//div[contains(@class,'flights-schedule-col')]/a/p[contains(@class,'lowFareAmt')]/text()")
            x = 0
            for amt in f:
                if (int(d[x]) == day):
                    print airport + ': ' + str(m[x]) + ' - ' + str(d[x]) + ' - ' + str(f[x])
                
                x = x + 1
            
            print('---------- REQUESTING NEXT DAY -------------')
            time.sleep(1)

threads = []
for airport in airports:
    t = threading.Thread(target=check_price, args=(airport,))
    threads.append(t)
    t.start()
